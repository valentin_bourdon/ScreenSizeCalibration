# Screen Size Calibration

Calculate the diagonal size of the screen in inches.
It's useful for example if you want to have the same physical size of an image  in multiple screen size and resolution.

## Requirements
* Unity 2020.3 or later.

## Installing the Package
* Add the name of the package (found in package.json) followed by the repository URL to your Unity project's manifest.json or using the package manager in Unity.


```json
{
  "dependencies": {
    "com.bourdonvalentin.screensizecalibration": "https://gitlab.com/valentin_bourdon/ScreenSizeCalibration.git",
}
```

## License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
